CC=gcc
EXECUTABLE=extract
CFLAGS=-O2 -Wall
LDFLAGS=

OBJS := \
	extract.o

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CC) $+ -o $@ $(LDFLAGS)

$(EXECUTABLE)-static: $(OBJS)
	$(CC) $+ -o $@ $(LDFLAGS) -static

%.o: %.c
	$(CC) $< -o $@ -c $(CFLAGS)

clean:
	rm -f *.o $(EXECUTABLE) $(EXECUTABLE)-static

.PHONY: all clean
