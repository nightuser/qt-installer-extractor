# Qt Installer Framework extractor

The program extracts installers generated with [Qt Installer Framework](https://doc.qt.io/qtinstallerframework/index.html). It might be helpful in several cases:

* Extract specific components and don't perform the whole installation.
* Extract the installer in a headless environment.

It doesn't have any dependencies outside the C library and Linux's `mkdir` function.

## Comparison with QT-CI

[QT-CI](https://github.com/benlau/qtci) also gives you ability to install binary Qt builds in a headless environment. However, it runs the installer in the minimal platform with JS-based automatization and this may not work in a specific scenarios. On the other hand, you wouldn't need applying the operations from `installscript.qs` (see below) manually.

## Usage

Firstly, compile the program with `make` and then run it with the following arguments:

```sh
./extract FILENAME OUTPUT_DIRECTORY
```

E.g.

```sh
./extract qt-opensource-linux-x64-5.9.9.run content
```

## TODO

* Also extract `QResource` files (every component has its own `installscript.qs`).
* Customize extract output (specified files, only list, etc).
* Do better error-handling.
