#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>

#include <sys/stat.h>

/* 4 MiB */
#define BUFFER_SIZE 4 * 1024 * 1024

#define MAGIC_COOKIE (uint64_t) 0xc2630a1c99d668f8ULL
const char * fname;
const char * out_dir;

struct collection_entry {
	char *name;
	uint64_t offset;
	uint64_t length;
};

void free_collection_entry(struct collection_entry *entry) {
	free(entry->name);
}

#define fread_uint64(DST, FH) fread(DST, sizeof(uint64_t), 1, FH)

static size_t fread_qbytearray(char **dst, FILE *fh) {
	uint64_t length;
	fread_uint64(&length, fh);
	*dst = (char *) malloc(length + 1);
	(*dst)[length] = '\0';
	return fread(*dst, 1, length, fh);
}

static size_t fread_collection_entry(struct collection_entry *entry, FILE *fh) {
	size_t read_bytes = 0;
	read_bytes += fread_qbytearray(&entry->name, fh);
	read_bytes += fread_uint64(&entry->offset, fh);
	read_bytes += fread_uint64(&entry->length, fh);
	return read_bytes;
}

static void extract_file(struct collection_entry *file, struct collection_entry *collection, uint64_t content_offset, FILE *fh) {
	long current_pos = ftell(fh);

	fseek(fh, content_offset + file->offset, SEEK_SET);

	size_t file_name_length = strlen(file->name);
	size_t collection_name_length = strlen(collection->name);
	size_t out_dir_length = strlen(out_dir);
	size_t out_name_length = out_dir_length + 1 + collection_name_length + 1 + file_name_length; 
	char *out_name = (char *) malloc(out_name_length + 1);
	size_t out_name_pos = 0;
#define add_string(STR) do { \
	memcpy(out_name + out_name_pos, STR, STR##_length); \
	out_name_pos += STR##_length; \
	out_name[out_name_pos] = '\0'; \
} while(0)
#define add_string2(STR) do { \
	memcpy(out_name + out_name_pos, STR->name, STR##_name_length); \
	out_name_pos += STR##_name_length; \
	out_name[out_name_pos] = '\0'; \
} while(0)
#define add_char(CHAR) do { \
	out_name[out_name_pos] = CHAR; \
	++out_name_pos; \
} while(0)
	add_string(out_dir);
	mkdir(out_name, 0777);
	add_char('/');
	add_string2(collection);
	mkdir(out_name, 0777);
	add_char('/');
	add_string2(file);
	assert(out_name_pos == out_name_length);

	printf("\t\tWriting to '%s'\n", out_name);

	FILE *outfh = fopen(out_name, "wb");

	size_t bytes_left = file->length;
	char buffer[BUFFER_SIZE];
	while (bytes_left > 0) {
		size_t chunk_size = (bytes_left > BUFFER_SIZE) ? BUFFER_SIZE : bytes_left;
		fread(buffer, 1, chunk_size, fh);
		fwrite(buffer, 1, chunk_size, outfh);
		bytes_left -= chunk_size;
	}

	fclose(outfh);

	free(out_name);
	fseek(fh, current_pos, SEEK_SET);
}

int main(int argc, char *argv[]) {
	if (argc != 3) {
		printf("Usage: %s FILENAME OUT_DIR\n", argv[0]);
		return 1;
	}

	fname = argv[1];
	out_dir = argv[2];

	int result = 0;

	FILE *f = fopen(fname , "rb");

	fseek(f, 0, SEEK_END);

	uint64_t guess;
	long end;
	int found = 0;
	while ((end = ftell(f)) >= sizeof(uint64_t)) {
		fseek(f, -sizeof(uint64_t), SEEK_CUR);
		fread_uint64(&guess, f);
		fseek(f, -sizeof(uint64_t), SEEK_CUR);

		if (guess == MAGIC_COOKIE) {
			printf("end = %ld\n", end);
			found = 1;
			break;
		}
	}

	if (!found) {
		fprintf(stderr, "Magic cookie %" PRIx64 " not found.\n", MAGIC_COOKIE);
		result = 1;
		goto cleanup;
	}

	fseek(f, -8 * 3, SEEK_CUR);
	uint64_t meta_data_count;
	fread_uint64(&meta_data_count, f);
	uint64_t content_size;
	fread_uint64(&content_size, f);

	uint64_t content_offset = end - content_size;

	fseek(f, end - (8 * 8 + (long) meta_data_count * 2 * 8), SEEK_SET);
	uint64_t collections_offset, collections_length;
	fread_uint64(&collections_offset, f);
	fread_uint64(&collections_length, f);

	printf("Collections: offset=%" PRIx64 " length=%" PRIx64 "\n", content_offset + collections_offset, collections_length);

	fseek(f, content_offset + collections_offset, SEEK_SET);
	uint64_t collections_count;
	fread_uint64(&collections_count, f);
	for (uint64_t i = 0; i < collections_count; ++i) {
		struct collection_entry collection;
		fread_collection_entry(&collection, f);

		printf("Collection '%s', offset=%" PRIx64 ", length=%"PRIx64 "\n", collection.name, content_offset + collection.offset, collection.length);

		long current_pos = ftell(f);
		fseek(f, content_offset + collection.offset, SEEK_SET);
		puts("Content:");
		uint64_t files_count;
		fread_uint64(&files_count, f);
		for (uint64_t j = 0; j < files_count; ++j) {
			struct collection_entry file;
			fread_collection_entry(&file, f);
			printf("\tFile '%s', offset=%" PRIx64 ", length=%"PRIx64 "\n", file.name, content_offset + file.offset, file.length);
			extract_file(&file, &collection, content_offset, f);
			free_collection_entry(&file);
		}
		fseek(f, current_pos, SEEK_SET);

		free_collection_entry(&collection);
	}

cleanup:
	fclose(f);
	return result;
}

/* vim: set noet sw=8 ts=8: */
